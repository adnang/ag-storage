using System;

namespace Ag.DataAccess.AzureStorage.App
{
    public class OptimisticConcurrencyException : Exception
    {
        public OptimisticConcurrencyException(Exception e) : base("Concurrency exception thrown", e)
        {
        }
    }
}