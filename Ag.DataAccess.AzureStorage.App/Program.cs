﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ag.DataAccess.AzureStorage.App
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var cosmosDataAccessConfiguration = new CosmosDataAccessConfiguration
            {
                ConnectionString = Environment.GetEnvironmentVariable("CosmosDb__ConnectionString"),
                Uri = new Uri(Environment.GetEnvironmentVariable("CosmosDb__Uri")),
                AccessKey = Environment.GetEnvironmentVariable("CosmosDb__AccessKey"),
                DatabaseName = "OrderBookedSvc",
                CollectionName = "State",
                PartitionKeyPath = "/OrderReference",
                Throughput = 400
            };

            ServicePointManager.DefaultConnectionLimit = 12;

            var cosmosDataAccess = new CosmosDataAccess(cosmosDataAccessConfiguration);
            var legacyCosmosDataAccess = new LegacyCosmosDataAccess(cosmosDataAccessConfiguration);

            await StartTest(cosmosDataAccess, "abc");
            Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            await StartTest(legacyCosmosDataAccess, "def");
        }

        private static async Task StartTest(IDataAccess dataAccess, string id)
        {
            try
            {
                await dataAccess.Init();
                await Task.WhenAll(
                    Enumerable.Range(0, 3)
                        .Select(i => Retry(() => TestDataAccess(dataAccess, i, id), i)));
            }
            finally
            {
                //await dataAccess.Reset();
            }
        }

        private static async Task Retry(Func<Task> inner, int instance, int retryCount = 3)
        {
            for (var i = 0; i < retryCount; i++)
            {
                try
                {
                    await inner();
                    break;
                }
                catch (OptimisticConcurrencyException)
                {
                    Console.WriteLine($"{instance} - Concurrency exception caught. Retrying.");
                }
            }
        }

        private static async Task TestDataAccess(IDataAccess dataAccess, int instance, string id = "O123")
        {
            Console.WriteLine($"{instance} - Starting Test");
            var stopwatch = new Stopwatch();

            stopwatch.Restart();
            var res = await dataAccess.Load<TestPayload>(id, new DataAccessOptions
            {
                PartitionKey = id
            }, CancellationToken.None);

            Console.WriteLine($"{instance} - Finished Load. Took: {stopwatch.Elapsed}");
            stopwatch.Reset();

//            foreach (var s in res.MetaData.Select(kv => $"{kv.Key}:{kv.Value}"))
//            {
//                Console.WriteLine(s);
//            }

            var testPayload = await DoSomeWork(instance, id, res);

            Console.WriteLine($"{instance} - Completed. Starting save.");
            stopwatch.Restart();
            await res.SaveFunc(testPayload, new DataAccessOptions {PartitionKey = id},
                CancellationToken.None);

            Console.WriteLine($"{instance} - Save completed. Took:{stopwatch.Elapsed}");
            stopwatch.Stop();
            Console.WriteLine($"{instance} - Finished Test");
        }

        private static async Task<TestPayload> DoSomeWork(int instance, string id, DataAccessResult<TestPayload> res)
        {
            Console.WriteLine(res.Value == null
                ? $"{instance} - Couldn't find a result for {id}"
                : $"{instance} - Found result for {id}");

            var testPayload = res.Value ?? new TestPayload();

            var next = new Random().Next();
            testPayload.Id = id;
            testPayload.OrderReference = id;
            testPayload.CustomerName = "Testing Now " + next;
            testPayload.ComplexObject.Active = true;

            Console.WriteLine($"{instance} - Doing some work...");
            await Task.Delay(500);
            return testPayload;
        }
    }

    [Serializable]
    public class TestPayload
    {
        [JsonProperty("id")] public string Id { get; set; }

        public string OrderReference { get; set; }

        public string CustomerName { get; set; }

        public ComplexObject ComplexObject { get; set; } = new ComplexObject();

        public List<ComplexObject> ComplexObjects { get; set; } = new List<ComplexObject>();
    }

    [Serializable]
    public class ComplexObject
    {
        public int Id { get; set; }
        public bool? Active { get; set; }
    }
}