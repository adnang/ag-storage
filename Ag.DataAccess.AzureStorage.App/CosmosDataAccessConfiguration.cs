using System;
using System.Security;

namespace Ag.DataAccess.AzureStorage.App
{
    public class CosmosDataAccessConfiguration
    {
        public string DatabaseName { get; set; }
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string PartitionKeyPath { get; set; }
        public int Throughput { get; set; } = 400;
        public Uri Uri { get; set; }
        public string AccessKey { get; set; }
    }
}