using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;

namespace Ag.DataAccess.AzureStorage.App
{
    public class CosmosDataAccess : IDataAccess
    {
        private readonly CosmosDataAccessConfiguration _configuration;
        private readonly CosmosContainer _container;
        private readonly CosmosClient _cosmosClient;

        public CosmosDataAccess(CosmosDataAccessConfiguration configuration)
        {
            _configuration = configuration;

            _cosmosClient = new CosmosClient(new CosmosConfiguration(_configuration.ConnectionString)
            {
                ConnectionMode = ConnectionMode.Direct,
            });

            _container = _cosmosClient.Databases[_configuration.DatabaseName]
                .Containers[_configuration.CollectionName];
        }

        public async Task Init()
        {
            Console.WriteLine("Initializing Database.");
            var dbRes = await _cosmosClient.Databases
                .CreateDatabaseIfNotExistsAsync(_configuration.DatabaseName);
            Console.WriteLine($"Create Database Request Charge: {dbRes.RequestCharge}");

            Console.WriteLine("Initializing Container.");
            var conRes =
                await dbRes.Database.Containers.CreateContainerIfNotExistsAsync(
                    new CosmosContainerSettings(_configuration.CollectionName, _configuration.PartitionKeyPath)
                    {
                        IndexingPolicy = new IndexingPolicy
                        {
                            IndexingMode = IndexingMode.None,
                            Automatic = false
                        }
                    }, _configuration.Throughput);

            Console.WriteLine($"Create Collection Request Charge: {conRes.RequestCharge}");
        }


        public async Task Reset()
        {
            Console.WriteLine("Deleting Database.");
            var dbRes = await _container.Database.DeleteAsync();
            Console.WriteLine($"Delete Database Request Charge: {dbRes.RequestCharge}");
        }

        public async Task<DataAccessResult<T>> Load<T>(string id, DataAccessOptions options, CancellationToken ct)
            where T : class
        {
            var response = await _container.Items.ReadItemAsync<T>(options.PartitionKey, id, null, ct);
            Console.WriteLine($"Read Request Charge: {response.RequestCharge}");

            var metaData = response.Headers.AllKeys().ToDictionary(k => k, s => response.Headers[s] as object);

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return new DataAccessResult<T>
                {
                    SaveFunc = NotFoundSaveFunc,
                    MetaData = metaData
                };
            }

            return new DataAccessResult<T>
            {
                Value = response.Resource,
                SaveFunc = CreateSaveFunc<T>(response.ETag),
                MetaData = metaData
            };
        }

        private async Task NotFoundSaveFunc<T>(T value, DataAccessOptions options, CancellationToken ct)
            where T : class
        {
            await ThrowOnConflict(async () =>
            {
                Console.WriteLine("Creating document.");
                var res = await _container.Items.CreateItemAsync(options.PartitionKey, value,
                    cancellationToken: ct);

                Console.WriteLine($"Create Request Charge: {res.RequestCharge}");
            });
        }

        private Func<T, DataAccessOptions, CancellationToken, Task> CreateSaveFunc<T>(string eTag)
            where T : class
        {
            return async (value, options, ct) =>
            {
                await ThrowOnConflict(async () =>
                {
                    Console.WriteLine($"Saving document with etag {eTag}");
                    var res = await _container.Items
                        .UpsertItemAsync(options.PartitionKey, value, new CosmosItemRequestOptions()
                        {
                            AccessCondition = new AccessCondition
                            {
                                Condition = eTag,
                                Type = AccessConditionType.IfMatch,
                            }
                        }, ct);

                    Console.WriteLine($"Upsert Request Charge: {res.RequestCharge}");
                });
            };
        }

        private static async Task ThrowOnConflict(Func<Task> inner)
        {
            try
            {
                await inner();
            }
            catch (CosmosException ex)
            {
                if (ex.StatusCode == HttpStatusCode.Conflict
                    || ex.StatusCode == HttpStatusCode.PreconditionFailed)
                {
                    throw new OptimisticConcurrencyException(ex);
                }

                throw;
            }
        }
    }
}