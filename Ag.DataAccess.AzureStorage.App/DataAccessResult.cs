using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ag.DataAccess.AzureStorage.App
{
    public sealed class DataAccessResult<T> where T : class
    {
        internal DataAccessResult()
        {
        }

        public T Value { get; set; }

        public IReadOnlyDictionary<string, object> MetaData { get; set; } = new Dictionary<string, object>();

        public Func<T, DataAccessOptions, CancellationToken, Task> SaveFunc { get; set; }
    }
}
