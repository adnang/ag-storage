namespace Ag.DataAccess.AzureStorage.App
{
    public class DataAccessOptions
    {
        public string PartitionKey { get; set; }
    }
}
