using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace Ag.DataAccess.AzureStorage.App
{
    public class LegacyCosmosDataAccess : IDataAccess
    {
        private readonly CosmosDataAccessConfiguration _configuration;
        private readonly DocumentClient _documentClient;

        public LegacyCosmosDataAccess(CosmosDataAccessConfiguration configuration)
        {
            _configuration = configuration;

            _documentClient = new DocumentClient(configuration.Uri,
                configuration.AccessKey,
                new ConnectionPolicy
                {
                    ConnectionMode = ConnectionMode.Direct
                });
        }

        public async Task Init()
        {
            Console.WriteLine("Initializing Legacy Database.");

            var dbRes = await _documentClient.CreateDatabaseIfNotExistsAsync(new Database
            {
                Id = _configuration.DatabaseName
            }, new RequestOptions
            {
                OfferThroughput = _configuration.Throughput
            });

            Console.WriteLine($"Create Database Request Charge: {dbRes.RequestCharge}");

            Console.WriteLine("Initializing Legacy Collection.");
            var res = await _documentClient.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(_configuration.DatabaseName),
                new DocumentCollection
                {
                    Id = _configuration.CollectionName,
                    PartitionKey = new PartitionKeyDefinition
                    {
                        Paths = {"/OrderReference"}
                    },
                    IndexingPolicy = new IndexingPolicy
                    {
                        Automatic = false,
                        IndexingMode = IndexingMode.None
                    }
                });
            Console.WriteLine($"Create Collection Request Charge: {res.RequestCharge}");
        }

        public async Task Reset()
        {
            Console.WriteLine("Deleting Legacy Database.");
            var databaseUri = UriFactory.CreateDatabaseUri(
                _configuration.DatabaseName);
            var res = await _documentClient.DeleteDatabaseAsync(databaseUri);
            Console.WriteLine($"Delete database Request Charge: {res.RequestCharge}");
        }

        public async Task<DataAccessResult<T>> Load<T>(string id, DataAccessOptions options, CancellationToken ct)
            where T : class
        {
            var requestOptions = CreateRequestOptions(options);

            var documentUri = UriFactory.CreateDocumentUri(
                _configuration.DatabaseName,
                _configuration.CollectionName,
                id);

            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(_configuration.DatabaseName,
                _configuration.CollectionName);

            try
            {
                var res = await _documentClient.ReadDocumentAsync<T>(documentUri, requestOptions, ct);
                var eTag = res.ResponseHeaders["etag"];
                Console.WriteLine($"Load into type latency: {res.RequestLatency}");
                Console.WriteLine($"Read Request Charge: {res.RequestCharge}");
                var value = res.Document;
                return new DataAccessResult<T>
                {
                    Value = value,
                    SaveFunc = CreateSaveFunc<T>(documentCollectionUri, eTag),
                    MetaData = res.ResponseHeaders.AllKeys.ToDictionary(k => k, k => res.ResponseHeaders[k] as object)
                };
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode != HttpStatusCode.NotFound)
                {
                    throw;
                }

                Console.WriteLine($"Not Found Request Charge: {e.RequestCharge}");
                return NotFoundDataAccessResult<T>(documentCollectionUri);
            }
        }

        private static RequestOptions CreateRequestOptions(DataAccessOptions options)
        {
            return new RequestOptions
            {
                PartitionKey = new PartitionKey(options.PartitionKey)
            };
        }

        private Func<T, DataAccessOptions, CancellationToken, Task> CreateSaveFunc<T>(Uri collectionUri, string eTag)
            where T : class
        {
            return async (value, options, ct) =>
            {
                await ThrowOnConflict(async () =>
                {
                    Console.WriteLine($"Saving document with etag {eTag}");
                    var requestOptions = CreateRequestOptions(options);
                    requestOptions.AccessCondition = new AccessCondition
                    {
                        Condition = eTag,
                        Type = AccessConditionType.IfMatch
                    };
                    var res = await _documentClient.UpsertDocumentAsync(collectionUri, value, requestOptions, true, ct);
                    Console.WriteLine($"Upsert Request Charge: {res.RequestCharge}");
                    Console.WriteLine($"Upsert Latency: {res.RequestLatency}");
                });
            };
        }

        private DataAccessResult<T> NotFoundDataAccessResult<T>(Uri collectionUri) where T : class
        {
            return new DataAccessResult<T>
            {
                SaveFunc = async (value, options, ct) =>
                {
                    await ThrowOnConflict(async () =>
                    {
                        var res = await _documentClient.CreateDocumentAsync(collectionUri,
                            value,
                            CreateRequestOptions(options),
                            disableAutomaticIdGeneration: true,
                            cancellationToken: ct);

                        Console.WriteLine($"Create doc Request Charge: {res.RequestCharge}");
                    });
                }
            };
        }

        private static async Task ThrowOnConflict(Func<Task> inner)
        {
            try
            {
                await inner();
            }
            catch (DocumentClientException ex)
            {
                if (ex.StatusCode == HttpStatusCode.Conflict
                    || ex.StatusCode == HttpStatusCode.PreconditionFailed)
                {
                    throw new OptimisticConcurrencyException(ex);
                }

                throw;
            }
        }
    }
}