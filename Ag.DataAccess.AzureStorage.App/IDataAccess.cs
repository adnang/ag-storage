using System.Threading;
using System.Threading.Tasks;

namespace Ag.DataAccess.AzureStorage.App
{
    public interface IDataAccess
    {
        Task Init();

        Task Reset();

        Task<DataAccessResult<T>> Load<T>(string id, DataAccessOptions options, CancellationToken ct)
            where T : class;
    }
}