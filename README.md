# Notes for CosmosDb
- Insert with If-Not-Exist semantics is achieved through the `CreateDocumentAsync` method, which throws `DocumentClientException` with StatusCode == 409 Conflict
- a lowercase Json property "id" needs to exist on the object being serialized, this can be achieved through the JsonProperty annotation